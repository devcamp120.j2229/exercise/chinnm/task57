package com.task573.restapi.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task573.restapi.model.CDrink;

@RestController
public class CDrinkController {
    @CrossOrigin
    @GetMapping("/devcamp-drinks")
    // khai báo danh sách
    public ArrayList<CDrink> getDrink() {
        ArrayList<CDrink> listDrink = new ArrayList<CDrink>();
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        // Khởi tạo đối tượng
        CDrink traTac = new CDrink(1, "TRATAC", "Trà tắc", 10000, today, today);
        CDrink coca = new CDrink(2, "COCA", "Cocacola", 15000, today, today);
        CDrink pepsi = new CDrink(3, "PEPSI", "Pepsi", 15000, today, today);
        CDrink lavie = new CDrink(4, "TRASUA", "Trà sữa trân châu", 40000, today, today);
        CDrink traSua = new CDrink(5, "TRASUA", "Trà sữa trân châu", 40000, today, today);
        CDrink fanta = new CDrink(6, "FANTA", "Fanta", 15000, today, today);

        // Thêm đối tượng vào danh sách
        listDrink.add(traTac);
        listDrink.add(coca);
        listDrink.add(pepsi);
        listDrink.add(lavie);
        listDrink.add(traSua);

        listDrink.add(fanta);

        return listDrink;
    }

}
